import { Component, ElementRef, ViewChild, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HousingLocationComponent } from '../housing-location/housing-location.component';
import { HousingLocation } from '../housinglocation';
import { HousingService } from '../housing.service';
import { WagertoolComponent } from '../wagertool/wagertool.component';


@Component({
  selector: 'app-home',
  standalone: true,
  imports: [
    CommonModule,
    HousingLocationComponent,
    WagertoolComponent
  ],

  templateUrl: `./home.component.html`,
  styleUrls: ['./home.component.css'],
})

export class HomeComponent {
  housingLocationList: HousingLocation[] = [];
  housingService: HousingService = inject(HousingService);
  filteredLocationList: HousingLocation[] = [];
  hasFilters: boolean = false;
  @ViewChild('filter') filterInput: ElementRef<HTMLInputElement> = {} as ElementRef;;

  constructor() {
    this.housingService.getAllHousingLocations().then((housingLocationList: HousingLocation[]) => {
      this.housingLocationList = housingLocationList;
      this.filteredLocationList = housingLocationList;
    });
  }

  filterResults(text: string): void {
    if (!text) {
      this.filteredLocationList = this.housingLocationList; // reset
      this.hasFilters = false;
      return;
    }
    // this.filteredLocationList = this.housingLocationList.filter(
    //   housingLocation => housingLocation.city.toLowerCase().includes(text.toLowerCase())
    //     || housingLocation.name.toLowerCase().includes(text.toLowerCase())

    // );
    // somaUmv2 = (numero: number): number => {
    //   const resultado = 1 + numero;
    //   return resultado;
    // }


    this.filteredLocationList = this.housingLocationList.filter(
      function (housingLocation) {
        const hasTextInCityName = housingLocation.city.toLowerCase().includes(text.toLowerCase());
        const hasTextHouseName = housingLocation.name.toLowerCase().includes(text.toLowerCase());
        return hasTextInCityName || hasTextHouseName;
      }
    );

    this.hasFilters = true;
  }

  /*somaUm(numero: number): number {
    const resultado = 1 + numero;
    return resultado;
  }

  somaUmv2 = (numero: number): number => {
    const resultado = 1 + numero;
    return resultado;
  }

  somaUmv3 = (numero: number): number => {
    return 1 + numero;
  }

  somaUmv4 = (numero: number): number => (1 + numero);
*/



  clearFilters(): void {
    //  const cenas=this.filterInput;
    //  debugger; 
    this.filterResults("");
    this.filterInput.nativeElement.value = "";
  }
}