import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HousingLocationComponent } from '../housing-location/housing-location.component';



@Component({
  selector: 'wagertool',
  standalone: true,
  imports: [
    CommonModule,
    HousingLocationComponent
  ],

  templateUrl: `./wagertool.component.html`,
  styleUrls: ['./wagertool.component.css'],
})

export class WagertoolComponent {


  constructor() {

  }
  highlightText(textId: string) {
    const textElements = document.querySelectorAll('.text-block2 p');
    textElements.forEach(text => {
      if (text.id === textId) {
        text.classList.add('highlight');
      } else {
        text.classList.add('lowlight');
      }
    });
    
  }

  resetTextHighlight(textId: string) {
    const textElements = document.querySelectorAll('.text-block2 p');
    textElements.forEach(text => {
      text.classList.remove('highlight');
      text.classList.remove('lowlight');
    });
  }

  addBlinkAnimation(circleId: string) {
    const circle = document.getElementById(circleId);
    if (circle) {
      circle.classList.add('blink-animation');
    }
  }

  removeBlinkAnimation(circleId: string) {
    const circle = document.getElementById(circleId);
    if (circle) {
      circle.classList.remove('blink-animation');
    }
  }
  

}