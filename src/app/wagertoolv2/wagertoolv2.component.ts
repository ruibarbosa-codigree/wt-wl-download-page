import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HousingLocationComponent } from '../housing-location/housing-location.component';

@Component({
  selector: 'app-wagertoolv2',
  standalone: true,
  imports: [],
  templateUrl: './wagertoolv2.component.html',
  styleUrl: './wagertoolv2.component.css'
})
export class Wagertoolv2Component {
  constructor() {

  }
  highlightText(textId: string) {
    const textElements = document.querySelectorAll('.textContainer div .topic');
    textElements.forEach(text => {
      if (text.id === textId) {
        text.classList.add('highlight');
      } else {
        text.classList.add('lowlight');
      }
    });
    
  }

  resetTextHighlight(textId: string) {
    const textElements = document.querySelectorAll('.textContainer div .topic');
    textElements.forEach(text => {
      text.classList.remove('highlight');
      text.classList.remove('lowlight');
    });
  }

  addBlinkAnimation(circleId: string) {
    const circle = document.getElementById(circleId);
    if (circle) {
      circle.classList.add('blink-animation');
    }
  }

  removeBlinkAnimation(circleId: string) {
    const circle = document.getElementById(circleId);
    if (circle) {
      circle.classList.remove('blink-animation');
    }
  }
 
  

}


