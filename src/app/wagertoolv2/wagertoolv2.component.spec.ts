import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Wagertoolv2Component } from './wagertoolv2.component';

describe('Wagertoolv2Component', () => {
  let component: Wagertoolv2Component;
  let fixture: ComponentFixture<Wagertoolv2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [Wagertoolv2Component]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(Wagertoolv2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
