import { Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { DetailsComponent } from './details/details.component';
import { WagertoolComponent } from './wagertool/wagertool.component';
import { Wagertoolv2Component } from './wagertoolv2/wagertoolv2.component';

const routeConfig: Routes = [
    {
      path: '',
      component: HomeComponent,
      title: 'Home page'
    },
    {
      path: 'details/:id',
      component: DetailsComponent,
      title: 'Home details'
    },
    {
      path: 'wagertool',
      component: WagertoolComponent,
      title: 'wagertool'
    },
    {
      path: 'wagertoolv2',
      component: Wagertoolv2Component,
      title: 'wagertoolv2'
    }
  ];
  
  export default routeConfig;